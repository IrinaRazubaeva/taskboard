package com.example.taskboard.controllers;

import com.example.taskboard.dto.TaskBoardRequestDto;
import com.example.taskboard.dto.TaskRequestDto;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("${app.domen}" + "task")
@ApiOperation("Task Api")
public class TaskController {

    @ApiOperation(value = "Create a new Task")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The Task was successfully added"),
    })
    @PostMapping("/create")
    public TaskRequestDto createTask(@RequestBody TaskRequestDto task){return task;}

    @ApiOperation(value = "Delete a Task")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The Task was successfully deleted"),
    })
    @DeleteMapping("/delete")
    public void deleteTask(){}

    @ApiOperation(value = "Update the Task")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The Task has been successfully updated"),
    })
    @PostMapping("/update")
    public void updateProject(){}

    @ApiOperation(value = "Show all Task entries")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Data received successfully"),
    })
    @GetMapping("/show_all")
    public void allProject(){}

    @ApiOperation(value = "Get a Task by id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Data received successfully"),
            @ApiResponse(code = 404, message = "Not found - The Task was not found")
    })
    @GetMapping("/show/{taskId}")
    public void idProject(){}
}
