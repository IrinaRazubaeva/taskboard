package com.example.taskboard.controllers;


import com.example.taskboard.dto.ProjectRequestDto;
import com.example.taskboard.dto.ProjectResponseDto;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.ApiOperation;


@RestController
@RequestMapping("${app.domen}" + "/project")
@ApiOperation("Project Api")
public class ProjectController {

    @ApiOperation(value = "Create a new Project")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The Project was successfully added"),
    })
    @PostMapping("/create")
    public ProjectRequestDto createProject(@RequestBody ProjectRequestDto project){
        return project;
    }

    @ApiOperation(value = "Delete a Project")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The Project was successfully deleted"),
    })
    @DeleteMapping("/delete")
    public void deleteProject(){}

    @ApiOperation(value = "Update the project")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The Project has been successfully updated"),
    })
    @PostMapping("/update")
    public void updateProject(){}

    @ApiOperation(value = "Show all project entries")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Data received successfully"),
   })
    @GetMapping("/show_all")
    public void allProject(){}

    @ApiOperation(value = "Get a Project by id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Data received successfully"),
            @ApiResponse(code = 404, message = "Not found - The Project was not found")
    })
    @GetMapping("/show/{projectId}")
    public void idProject() {
    }

}
