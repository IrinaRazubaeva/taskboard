package com.example.taskboard.controllers;

import com.example.taskboard.dto.EmployeeRequestDto;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("${app.domen}" + "/employee")
@ApiOperation("Employee Api")
public class EmployeeController {

    @ApiOperation(value = "Add a new employee record")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The employee was successfully added"),
    })
    @PostMapping("/add")
    public EmployeeRequestDto addEmployee(@RequestBody EmployeeRequestDto employee){return employee;}

    @ApiOperation(value = "Delete an employee record")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The employee was successfully deleted"),
    })
    @DeleteMapping("/delete")
    public void deleteProject(){}

    @ApiOperation(value = "Update employee record")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The employee has been successfully updated"),
    })
    @PostMapping("/update")
    public void updateProject(){}

    @ApiOperation(value = "Show all employee records")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Data received successfully"),
    })
    @GetMapping("/show_all")
    public void allProject(){}
}
