package com.example.taskboard.controllers;

import com.example.taskboard.dto.TaskBoardRequestDto;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("${app.domen}" + "board")
@ApiOperation("TaskBoard Api")
public class TaskBoardController {

    @ApiOperation(value = "Create a new TaskBoard")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The TaskBoard was successfully added"),
    })
    @PostMapping("/create")
    public TaskBoardRequestDto createTaskBoard(@RequestBody TaskBoardRequestDto taskBoard){return taskBoard;}

    @ApiOperation(value = "Delete a TaskBoard")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The TaskBoard was successfully deleted"),
    })
    @DeleteMapping("/delete")
    public void deleteTaskBoard(){}

    @ApiOperation(value = "Update the TaskBoard")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The TaskBoard has been successfully updated"),
    })
    @PostMapping("/update")
    public void updateTaskBoard(){}

    @ApiOperation(value = "Show all TaskBoard entries")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Data received successfully"),
    })
    @GetMapping("/show_all")
    public void allTask(){}

    @ApiOperation(value = "Get a TaskBoard by id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Data received successfully"),
            @ApiResponse(code = 404, message = "Not found - The TaskBoard was not found")
    })
    @GetMapping("/show/{boardId}")
    public void idTaskBoard(){}
}
