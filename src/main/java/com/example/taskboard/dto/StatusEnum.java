package com.example.taskboard.dto;

public enum StatusEnum {
    BACKLOG,
    IN_PROGRESS,
    DONE
}
