package com.example.taskboard.dto;


import io.swagger.annotations.ApiModelProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
@Schema(description = "The essence of the task")
public class TaskResponseDto {
    @Schema(description = "Task ID", example = "1", required = true)
    private Integer id;
    @Schema(description = "Task status", example = "StatusEnum status", required = true)
    private Enum<StatusEnum> status;
    @Schema(description = "Task purpose", example = "Write the first chapter", required = true)
    private String purpose;
    @Schema(description = "Author ID", example = "1", required = true)
    private Integer authorId;
    @Schema(description = "Executor ID", example = "1", required = true)
    private Integer executorId;
    @Schema(description = "ReleaseVersion ID", example = "1", required = true)
    private Integer versionId;
    @Schema(description = "Field for additional notes", example = "Any information")
    private String note;
}
