package com.example.taskboard.dto;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Schema(description = "Version Information")
public class ReleaseVersionRequestDto {
    @Schema(description = "Version ID", example = "1", required = true)
    private Integer id;
    @Schema(description = "Version name", example = "v2.5", required = true)
    private String name;
    @Schema(description = "Start time of execution", example = "10.07.22", required = true)
    private LocalDateTime start;
    @Schema(description = "Completion time", example = "10.07.22", required = true)
    private LocalDateTime stop;
}
