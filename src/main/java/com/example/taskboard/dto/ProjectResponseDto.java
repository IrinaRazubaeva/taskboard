package com.example.taskboard.dto;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
@Schema(description = "The essence of the project")
public class ProjectResponseDto {
    @Schema(description = "Project ID", example = "1", required = true)
    private Integer id;
    @Schema(description = "Project status", example = "StatusEnum status", required = true)
    private Enum<StatusEnum> status;
    @Schema(description = "Project name", example = "Book", required = true)
    private String name;
    @Schema(description = "Project purpose", example = "Write a book", required = true)
    private String purpose;
    @Schema(description = "Project budget", example = "10000", required = true)
    private Integer budget;
}
