package com.example.taskboard.dto;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
@Schema(description = "The essence of the employee")
public class EmployeeRequestDto {
    @Schema(description = "Employee ID", example = "1", required = true)
    private Integer id;
    @Schema(description = "Employee first name", example = "Ivan", required = true)
    private String firstName;
    @Schema(description = "Employee second name", example = "Ivanov", required = true)
    private String secondName;

}
