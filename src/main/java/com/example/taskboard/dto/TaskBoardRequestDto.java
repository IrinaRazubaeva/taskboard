package com.example.taskboard.dto;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
@Schema(description = "The essence of the Task Board")
public class TaskBoardRequestDto {
    @Schema(description = "Task Board ID", example = "1", required = true)
    private Integer id;
    @Schema(description = "Task Board ID", example = "First chapter", required = true)
    private String name;
    @Schema(description = "Task ID", example = "1", required = true)
    private Integer idTask;
}
