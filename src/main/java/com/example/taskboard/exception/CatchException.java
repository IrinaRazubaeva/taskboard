package com.example.taskboard.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class CatchException extends RuntimeException {
    public CatchException(String message) {
        super(message);
    }
}
